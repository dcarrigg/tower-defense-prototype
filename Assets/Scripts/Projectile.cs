﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    // TODO: This class can represent everything about a projectile
    [SerializeField]
    public float damage = 5.0f;

    private void Start()
    {
        Destroy(gameObject, 10.0f);
    }

    public float GetDamange()
    {
        return damage;
    }

    /*
    private void OnCollisionEnter(Collision collision)
    {
        // TODO: Some better despawn logic for projectiles
        Destroy(gameObject);
    }
    */
}
