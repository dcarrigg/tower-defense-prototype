﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewWave", menuName = "Create New Wave")]
public class WaveDefinition : ScriptableObject
{
    // Collection of what enemies to spawn
    public EnemyDefinition[] enemiesToSpawn;

    // Weights for the various enemies to spawn
    public int[] enemySpawnWeights;
    int totalWeights = 0;

    // How many enemies to spawn before the boss
    public int totalEnemies = 40;
    int currentEnemyCount = 0;
    float nextSpawnTime = 0;
    public float minSpawnTime = 1.0f;
    public float maxSpawnTime = 5.0f;

    // The boss
    public bool shouldSpawnBoss = false;
    public EnemyDefinition boss;

    // Set up any initial data for this wave
    public void Initialize()
    {
        currentEnemyCount = 0;
        totalWeights = 0;
        foreach (int weight in enemySpawnWeights)
        {
            totalWeights += weight;
        }
    }

    // Check if an enemy should currently spawn
    // Note: This should be called from an update function
    public void CheckSpawn(Transform[] aPath)
    {
        nextSpawnTime -= Time.deltaTime;

        // Lets get some enemies spawning
        if (currentEnemyCount < totalEnemies && nextSpawnTime <= 0)
        {
            nextSpawnTime = Random.Range(minSpawnTime, maxSpawnTime);

            // Spawn an enemy
            int randWeight = Random.Range(1, totalWeights + 1);
            for (int i = 0; i < enemySpawnWeights.Length; i++)
            {
                randWeight -= enemySpawnWeights[i];
                if (randWeight <= 0)
                {
                    GameObject newEnemy = Instantiate(enemiesToSpawn[i].prefab, aPath[0].position, aPath[0].rotation);
                    newEnemy.GetComponent<Enemy>().SetWaypoints(aPath);
                    newEnemy.GetComponent<Enemy>().definition = enemiesToSpawn[i];
                    currentEnemyCount++;
                    break;
                }
            }
        }
        else if (currentEnemyCount >= totalEnemies && shouldSpawnBoss)
        {
            GameObject newEnemy = Instantiate(boss.prefab, aPath[0].position, aPath[0].rotation);
            newEnemy.GetComponent<Enemy>().SetWaypoints(aPath);
            newEnemy.GetComponent<Enemy>().definition = boss;
            currentEnemyCount++;
            shouldSpawnBoss = false;
        }
    }

    // Is this wave done spawning enemies
    public bool IsDone()
    {
        return currentEnemyCount == (shouldSpawnBoss ? totalEnemies + 1 : totalEnemies);
    }
}
