﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewEnemy", menuName = "Create New Enemy")]
public class EnemyDefinition : ScriptableObject
{
    // Max health for this enemy
    public float maxHealth;

    // The prefab to spawn for this enemy
    public GameObject prefab;

    // The amount of money the player gains when this enemy dies
    public int reward = 0;

    // How fast does this enemy move
    public float movementSpeed = 1.0f;
}
