﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewTower", menuName = "Create New Tower")]
public class TowerDefinition : ScriptableObject
{
    // For previewing the placement of the tower
    public GameObject previewPrefab;

    // Prefab to spawn when the tower is built
    public GameObject prefab;

    // Icon for the tower in the menu system
    public Sprite icon;

    // Projecticle this tower shoots
    public GameObject projectilePrefab;

    // The cost to build this tower
    public int cost;

    // Max radius of this turret
    public float targetRadius;

    // Rotation speed of the turret
    public float rotationSpeed = 140.0f;

    // How long between shots
    public float fireDelay = 0.5f;

    // Damage the projectile does
    public float projectileDamage = 5.0f;

    // Speed of the projectile
    public float projectileSpeed = 5.0f;
}
