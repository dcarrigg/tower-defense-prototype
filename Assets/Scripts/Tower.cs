﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
    // The part of the tower that rotates
    [SerializeField]
    Transform turret;

    // The definition for this tower
    public TowerDefinition definition;

    float lastFireTime = 0;

    // The current target the turret is looking at 
    Vector3 currentTarget;

    private void Awake()
    {
        turret = transform.Find("TurretObj");
    }

    void Update()
    {
        // Check if there is an enemy within range
        Collider[] enemies = Physics.OverlapSphere(transform.position, definition.targetRadius, 1 << LayerMask.NameToLayer("Enemy"));

        if (enemies.Length > 0)
        {
            // Aim at the enemy
            currentTarget = enemies[0].transform.position;
        }
        else
        {
            currentTarget = turret.position + Vector3.forward;
        }

        // Rotate the turret to look at the enemy
        Quaternion lookRotation = Quaternion.LookRotation(currentTarget - turret.position, Vector3.up);
        turret.rotation = Quaternion.RotateTowards(turret.rotation, lookRotation, definition.rotationSpeed * Time.deltaTime);

        // Do a raycast to see if the enemy is in front of the turret
        if (Time.realtimeSinceStartup - lastFireTime > definition.fireDelay)
        {
            RaycastHit hitInfo;
            if (Physics.Raycast(turret.transform.position, turret.forward, out hitInfo, definition.targetRadius, 1 << LayerMask.NameToLayer("Enemy")))
            {
                // Fire a projectile
                GameObject newProj = Instantiate(definition.projectilePrefab, turret.position, Quaternion.identity);
                newProj.GetComponent<Projectile>().damage = definition.projectileDamage;
                newProj.GetComponent<Rigidbody>().velocity = definition.projectileSpeed * turret.forward;
                lastFireTime = Time.realtimeSinceStartup;
            }
        }
    }

    // Draw turret debug info
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        // Draw the targeting radius
        Gizmos.DrawWireSphere(transform.position, definition.targetRadius);
        // Draw where the turret is currently targeting
        Gizmos.DrawLine(turret.position, currentTarget);
    }
}
