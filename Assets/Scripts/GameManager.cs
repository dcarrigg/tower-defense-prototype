﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    Transform[] waypoints;

    [SerializeField]
    TowerDefinition[] towerDefinitions;
    [SerializeField]
    Image[] buttonImages;
    [SerializeField]
    Text currencyDisplay;

    [SerializeField]
    // The current tower definition selected for building
    TowerDefinition selectedTower;
    GameObject towerPreview = null;

    [SerializeField]
    // Display for placing a turret to represent the firing radius
    GameObject radiusPreviewPrefab;
    GameObject radiusPreview = null;

    //[SerializeField]
    //EnemyDefinition[] enemiesToSpawn;
    
    [SerializeField]
    WaveDefinition[] waves;
    int currentWave = 0;

    [SerializeField]
    int initialCurrency = 100;
    // Keep track of how much money the player has to built turrets
    int currency = 0;

    static public GameManager instance;

    private void Start()
    {
        if (instance != null)
        {
            Destroy(gameObject);
            return;
        }
        instance = this;

        // Update the UI panels with the proper Towers
        int i = 0;
        for (; i<towerDefinitions.Length && i < 4; i++)
        {
            buttonImages[i].sprite = towerDefinitions[i].icon;
        }
        // Disable extra tower build buttons
        while(i<4)
        {
            buttonImages[i].gameObject.SetActive(false);
            i++;
        }

        // Set the initial currency
        ChangeCurrency(initialCurrency);

        if (waves.Length > 0)
            waves[0].Initialize();
    }

    void Update()
    {
        // Check if the current wave should spawn
        if (currentWave < waves.Length)
        {
            waves[currentWave].CheckSpawn(waypoints);
            if (waves[currentWave].IsDone() && GameObject.FindGameObjectsWithTag("Enemy").Length == 0)
            {
                currentWave++;
                waves[currentWave].Initialize();
            }
        }
        
        // Check mouse position for tower placement
        bool overTowerArea = false;
        Vector3 towerAreaPosition = Vector3.zero;
        Ray mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit mouseHit;
        if (Physics.Raycast(mouseRay, out mouseHit, 1000.0f, 1 << LayerMask.NameToLayer("TowerPlatform")))
        {
            // Check if we hit a location to build a turret
            if (mouseHit.collider.gameObject.CompareTag("TowerPlatform"))
            {
                overTowerArea = true;
                towerAreaPosition = mouseHit.point;
            }
        }

        // If we are over a valid tower placement location
        if (overTowerArea)
        {
            // Update the tower preview object
            if (towerPreview == null)
            {
                towerPreview = Instantiate(selectedTower.previewPrefab);
            }
            if (radiusPreview == null)
            {
                radiusPreview = Instantiate(radiusPreviewPrefab);
                radiusPreview.transform.localScale = new Vector3(selectedTower.targetRadius * 2, 1.0f, selectedTower.targetRadius * 2);
            }
            towerPreview.transform.position = towerAreaPosition;
            radiusPreview.transform.position = towerAreaPosition;

            // Check if we should build a tower
            if (Input.GetMouseButtonDown(0))
            {
                if (currency >= selectedTower.cost)
                {
                    // Spawn the new tower, add the Tower script if it doesn't have one
                    GameObject newTower = Instantiate(selectedTower.prefab, mouseHit.point, Quaternion.identity);
                    Tower towerScript = newTower.GetComponent<Tower>();
                    if (towerScript == null)
                    {
                        towerScript = newTower.AddComponent<Tower>();
                    }
                    newTower.GetComponent<Tower>().definition = selectedTower;
                    ChangeCurrency(-1*selectedTower.cost);
                }
            }
        }
        else
        {
            // Check if we need to destroy the tower preview
            if (towerPreview != null)
            {
                Destroy(towerPreview);
                Destroy(radiusPreview);
            }
            radiusPreview = null;
            towerPreview = null;
        }
    }

    public void ChangeCurrency(int aAmt)
    {
        currency += aAmt;
        currencyDisplay.text = currency.ToString();
    }

    // Set which tower the player wants to build
    public void SelectTowerToBuild(int aTowerIndex)
    {
        selectedTower = towerDefinitions[aTowerIndex];
    }
}
