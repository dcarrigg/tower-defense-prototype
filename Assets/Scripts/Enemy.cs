﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    // Where should the enemy move
    Transform[] waypoints;

    // The definition of this enemy
    public EnemyDefinition definition;

    [SerializeField]
    float currentHealth = 0.0f;

    [SerializeField]
    RectTransform healthMask;

    // Time it should take between each waypoint
    float totalTime = 2.0f;
    float currentTime;
    int currentWaypoint = 0;

    private void Start()
    {
        SetHealth(definition.maxHealth);
    }

    // Set the waypoints for this enemy
    public void SetWaypoints(Transform[] aWaypoints)
    {
        waypoints = aWaypoints;
    }

    // Update the enemy (simple movement)
    private void Update()
    {
        if (waypoints == null)
            return;

        // Move along the waypoints
        currentTime += Time.deltaTime * definition.movementSpeed;
        
        // Simple linear interpolation
        // TODO: Totaltime based on a speed + distance
        if (currentTime >= totalTime)
        {
            currentWaypoint++;
            currentTime -= totalTime;

            // Normalize total time
            Vector3 start = waypoints[currentWaypoint].position;
            Vector3 end = waypoints[currentWaypoint + 1].position;
            Vector3 distance = end - start;
            totalTime = distance.magnitude;
        }

        // Interpolation
        if (currentWaypoint < waypoints.Length-1)
        {
            Vector3 start = waypoints[currentWaypoint].position;
            Vector3 end = waypoints[currentWaypoint+1].position;
            Vector3 distance = end - start;
            transform.position = start + (currentTime / totalTime) * distance;
        }
    }

    // Set the enemy health to a specific value
    public void SetHealth(float aAmt)
    {
        currentHealth = aAmt;
        if (currentHealth > definition.maxHealth)
            currentHealth = definition.maxHealth;
        if (currentHealth < 0)
            currentHealth = 0;

        if (currentHealth <= 0)
            Destroy(gameObject);

        UpdateHealthBar();
    }

    // Deal damage to this enemy
    public void TakeDamage(float aAmt)
    {
        currentHealth -= aAmt;

        if (currentHealth <= 0)
        {
            Destroy(gameObject);
            currentHealth = 0;

            GameManager.instance.ChangeCurrency(definition.reward);
        }
        UpdateHealthBar();
    }

    // Update the health bar
    // Note: This should be called whenever the currentHealth changes
    void UpdateHealthBar()
    {
        healthMask.transform.localScale = new Vector3(currentHealth / definition.maxHealth, 1.0f, 1.0f);
    }
    
    // Check for collisions
    private void OnCollisionEnter(Collision collision)
    {
        // Check if a projectile just hit this enemy
        Projectile proj = collision.gameObject.GetComponent<Projectile>();
        if (proj != null)
        {
            TakeDamage(proj.GetDamange());

            // Destroy the projectile
            Destroy(proj.gameObject);
        }
        

        // TODO: This is also where we can check if we hit the base
    }

    // Draw enemy debug info
    private void OnDrawGizmos()
    {
        if (waypoints != null && currentWaypoint < waypoints.Length - 1)
        {
            Gizmos.color = Color.white;
            Gizmos.DrawLine(waypoints[currentWaypoint].transform.position, waypoints[currentWaypoint + 1].transform.position);
        }
    }
}
